using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public struct Vector
    {

        public double x;
        public double y;

        public Vector(double x0, double y0)
        {
            x = x0;
            y = y0;
        }

        public static double Scalar(Vector a, Vector b)
        {
            return (double) Math.Sqrt(a.x * b.x + a.y * b.y);
        }

        public static double Magnitude(Vector a)
        {
            return Scalar(a, a);
        }

        public static Vector operator *(Vector a, double b)
        {
            return new Vector(a.x * b, a.y * b);
        }
        public static Vector operator *( double b,Vector a)
        {
            return new Vector(a.x * b, a.y * b);
        }

        public static Vector operator /(Vector a, double b)
        {
            return new Vector(a.x / b, a.y / b);
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.x + b.x, a.y + b.y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.x - b.x, a.y - b.y);
        }


        public Vector normalized
        {
            get { return this / Magnitude(this); }
        }

        public double magnitude
        {
            get { return Magnitude(this); }
        }

    }
}