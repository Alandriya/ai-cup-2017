﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders;


namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public static partial class Controller
    {
        
        public static World CurrentWorld;
        public static Game CurrentGame;
        public static Player CurrentPlayer;
        
        public static Vector UpperLeftCorner = new Vector(0,0);
        public static Vector LowerRightCorner = new Vector(0,0);
        public static Vector LowerLeftCorner = new Vector(0, 0);
        public static Vector UpperRightCorner = new Vector(0, 0);
        public static Vector Center = new Vector(0, 0);
        public static Dictionary<long, Vehicle> Vehicles = new Dictionary<long, Vehicle>();
        public static TerrainType[][] TerrainMap;
        public static WeatherType[][] WeatherMap;
        public static List<GroupOrder> GroupOrderExecution = new List<GroupOrder>();



        public static void UpdateWorld()
        {
            TerrainMap = CurrentWorld.TerrainByCellXY;
            WeatherMap = CurrentWorld.WeatherByCellXY;
        }

        public static void Sync(ref World w, ref Game g, ref Player p)
        {
            CurrentGame = g;
            CurrentPlayer = p;
            CurrentWorld = w;
            LowerRightCorner = new Vector(CurrentWorld.Width,CurrentWorld.Height);
            LowerLeftCorner = new Vector(0, CurrentWorld.Height);
            UpperRightCorner = new Vector(CurrentWorld.Width, 0);

            if (CurrentWorld.TickIndex==0)
                UpdateWorld();

            Center = new Vector(CurrentWorld.Width/2, CurrentWorld.Height/2);

            foreach (Vehicle v in CurrentWorld.NewVehicles)
            {
                Vehicles.Add(v.Id,v);
            }

           
            List<int> MotionList = new List<int>();
            foreach(Group i in Group.GroupCollection)
                MotionList.Add(0);
            foreach (VehicleUpdate v in CurrentWorld.VehicleUpdates)
            {
                if (v.Durability <= 0)
                {
                    Vehicles.Remove(v.Id);
                }
                else
                {
                   
                 
                    if (Math.Pow(Vehicles[v.Id].X-v.X,2)+Math.Pow(Vehicles[v.Id].Y-v.Y,2)>0)
                    {
                        if (Vehicles[v.Id].PlayerId == CurrentPlayer.Id)
                        {
                            foreach (int i in Vehicles[v.Id].Groups)
                            {

                                MotionList[i - 1]++;
                            }

                           
                        }
                    }
                    else
                    {

                       
                    }
                   
                    Vehicles[v.Id] = new Vehicle(Vehicles[v.Id],v);
                }
               
            }
            
            foreach (Group i in Group.GroupCollection)
            {
                if (MyStrategy.TurnAvailable)
                {
                   // Console.WriteLine("{0} ticks with no motion in group {1}", i.TicksWithoutMotion, i.Id);
                    if (MotionList[i.Id - 1] == 0)
                        i.TicksWithoutMotion++;
                    else
                    {
                        i.TicksWithoutMotion = 0;
                        

                    }
                    if (i.TicksWithoutMotion > 10)
                    {
                        i.Motionless = true;
                        i.TicksWithoutMotion = 0;
                    }
                    else
                    {
                        i.Motionless = false;

                    }
                }

                i.OnTick();
            
      
            }

            List<GroupOrder> sorted = GroupOrderExecution.OrderBy(o => o.Ordering).ToList();
            foreach (GroupOrder r in sorted)
            {
                r.Execute();
            }
            if(sorted.Count>0)
                Console.WriteLine("{0} orders were send for execution in tick {1}",sorted.Count,CurrentWorld.TickIndex);
            GroupOrderExecution.Clear();


        }
        
        
        





        public static void CreateGroup(Vector ul, Vector lr, int num)
        {
            ClearAndSelect(ul,lr);
            Assign(num);
           
        }
        public static void CreateGroup(Vector ul, Vector lr, VehicleType t,int num)
        {
            ClearAndSelect(ul,lr,t);
            Assign(num);
        }
        
        public static void CreateGroup(Vector ul, Vector lr, VehicleType[] t,int num)
        {
            if(t.Length>0)
            ClearAndSelect(ul,lr,t[0]);
            if (t.Length > 1)
            {
                for(int i =1; i< t.Length;i++)
                    AddToSelection(ul, lr, t[i]);
            }
            Assign(num);
        }
        
        public static void CreateGroup(Group[] gr,int num)
        {
            if(gr.Length>0)
                ClearAndSelect(UpperLeftCorner,LowerRightCorner,gr[0].Id);
            if (gr.Length > 1)
            {
                for(int i =1; i< gr.Length;i++)
                    AddToSelection(UpperLeftCorner,LowerRightCorner, gr[i].Id);
            }
            Assign(num);
        }
        
        public static void MoveGroup(int num, Vector point,double maxspeed=0)
        {
            SelectGroup(num);
            Move(point,maxspeed);
        }
        
        public static void ScaleGroup(int num, Vector point,double fac,double maxspeed=0)
        {
            SelectGroup(num);
            Scale(fac,point,maxspeed);
        }
        
        public static void RotateGroup(int num, Vector point,double angle, double maxspeed =0, double maxangspeed = 0)
        {
            SelectGroup(num);
            Rotate(angle,point,maxspeed,maxangspeed);
        }

        public static List<Vehicle> GetVehiclesInGroup(int num)
        {

            List<Vehicle> temp = new List<Vehicle>();
           
            foreach (Vehicle v in Vehicles.Values)
            {
               
                if(v.Groups.Contains((num)))
                    temp.Add(v);
                
            }
            return temp;
        }
        
        
        private static void NuclearStrike(Vector point, long id)
        {
            MyStrategy.AddOrder(new NuclearStrikeOrder(id,point));
        }
        
        private static void ClearAndSelect(Vector ul, Vector lr, VehicleType tp, int gr = -1)
        {
            MyStrategy.AddOrder(new ClearAndSelectOrder(ul, lr, tp, gr));
        }
        private static void ClearAndSelect(Vector ul, Vector lr, int gr = -1)
        {
            
            MyStrategy.AddOrder(new ClearAndSelectOrder(ul, lr, gr));
        }
        
        private static void AddToSelection(Vector ul, Vector lr, VehicleType tp, int gr = -1)
        {
            MyStrategy.AddOrder(new AddToSelectionOrder(ul, lr, tp, gr));
        }
        private static void AddToSelection(Vector ul, Vector lr, int gr = -1)
        {
            MyStrategy.AddOrder(new AddToSelectionOrder(ul, lr, gr));
        }
        private static void Deselect(Vector ul, Vector lr, VehicleType tp, int gr = -1)
        {
            MyStrategy.AddOrder(new DeselectOrder(ul, lr, tp, gr));
        }
        private static void Deselect(Vector ul, Vector lr, int gr = -1)
        {
            MyStrategy.AddOrder(new DeselectOrder(ul, lr, gr));
        }

        private static void Move(Vector pos , double ms = -1)
        {
            MyStrategy.AddOrder(new MoveOrder(pos, ms));
        }

        private static void Assign(int num)
        {
            Group.GroupSelected = num;
            MyStrategy.AddOrder(new AssignOrder(num));
            
        }

        public static void SelectGroup(int num)
        {

            if (num != Group.GroupSelected)
            {
                Group.GroupSelected = num;
                ClearAndSelect(UpperLeftCorner, LowerRightCorner, num);
            }

        }
        
        private static void Disband(int num)
        {
            MyStrategy.AddOrder(new AssignOrder(num));
        }
        private static void Dismiss(int num)
        {
            MyStrategy.AddOrder(new AssignOrder(num));
        }

        private static void Rotate(double ang, Vector pt, double max = 0, double maxas = 0)
        {
            MyStrategy.AddOrder(new RotateOrder(ang,pt,max,maxas));
        }
        private static void Scale(double fac, Vector pt, double max = 0)
        {
            MyStrategy.AddOrder(new ScaleOrder(fac,pt,max));
        }
        public static CellInfo GetMapType(Vector position)
        {
            return new CellInfo(TerrainType.Plain, WeatherType.Clear); //КОСТЫЛЬ
            TerrainType Terrain = TerrainMap[(int)(position.x / 32)][(int)(position.y / 32)];
            WeatherType Weather = WeatherMap[(int)(position.x / 32)][(int)(position.y / 32)];
            return new CellInfo(Terrain, Weather);
        }
    }
}