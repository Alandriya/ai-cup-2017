﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Web;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public static partial class Controller
    {

        private static double[,] MakeHealthMap()
        {
            int size = 64;
            double[,] healthmap = new double[size, size];
            double[,] Sum = new double[size, size];
            int[,] Num = new int[size, size];
            for (var i = 0; i < size; i++)
                for (var j = 0; j < size; j++)
                {
                    Sum[i, j] = 0.0;
                    Num[i, j] = 0;
                }
            foreach (Vehicle v in Vehicles.Values)
            {
                if (v.PlayerId != 1) //if is enemy
                {
                    Sum[(int)(v.X / (1024/size)), (int)(v.Y / (1024/size))] += v.Durability;
                    Num[(int)(v.X / (1024/size)), (int)(v.Y / (1024/size))]++;
                }
            }
            for (var i = 0; i < size; i++)
                for (var j = 0; j < size; j++)
                {
                    if (Num[i, j] > 0)
                        healthmap[i, j] = Sum[i, j] / Num[i, j];
                    else
                        healthmap[i, j] = 0.0;
                }
            return healthmap;
        }
        private static void UpdateHealthMap(ref double[,] HealthMap)
        {
            int size = 64;
            double[,] Sum = new double[size, size];
            int[,] Num = new int[size, size];
            for (var i = 0; i < size; i++)
                for (var j = 0; j < size; j++)
                {
                    Sum[i, j] = 0.0;
                    Num[i, j] = 0;
                }
            foreach (Vehicle v in Vehicles.Values)
            {
                if (v.PlayerId != 1) //if is enemy
                {
                    Sum[(int)(v.X / (1024 / size)), (int)(v.Y / (1024 / size))] += v.Durability;
                    Num[(int)(v.X / (1024 / size)), (int)(v.Y / (1024 / size))]++;
                }
            }
            for (var i = 0; i < size; i++)
                for (var j = 0; j < size; j++)
                {
                    if (Num[i, j] > 0)
                        HealthMap[i, j] = Sum[i, j] / Num[i, j];
                    else
                        HealthMap[i, j] = 0.0;
                }
            return;
        }
        private static Coord GetMaxIndex()
        {
            double maxhp = 0.0;
            int maxi = 0;
            int maxj = 0;
            for (int i = 0; i < 64; i++)
            {
                for (int j = 0; j < 64; j++)
                {
                    if (HealthMap[i, j] > maxhp)
                    {
                        maxhp = HealthMap[i, j];
                        maxi = i;
                        maxj = j;
                    }
                }
            }
            return new Coord(maxi, maxj);
        }

    }
}
