using System;
using System.Collections.Generic;
using System.Linq;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public class Group
    {
        private static int IdCounter = 1;
        public int Id = 1;
        public Vector Destination;
        public double Speed;
        public bool HoldFormation;
        public static int GroupSelected = 0;
        public int TicksWithoutMotion = 0;
        public static List<Group> GroupCollection = new List<Group>();

        private Vector _center;
        private List<Vehicle> _units;
        
        public bool Motionless = false;

        public GroupOrder CurrentOrder;
        
        public Queue<GroupOrder> Orders = new Queue<GroupOrder>();
       

        public Group(Vector ul, Vector lr,bool hf = true)
        {
            Id = IdCounter;
            Console.WriteLine("{0} group created from {1} available",Id,Controller.CurrentGame.MaxUnitGroup);
            IdCounter++;
            Controller.CreateGroup(ul, lr, Id);
            GroupCollection.Add(this);
            HoldFormation = hf;

        }


        public Group(Vector ul, Vector lr, VehicleType t, bool hf = true)
        {
            Id = IdCounter;
            IdCounter++;
            Controller.CreateGroup(ul, lr, t,Id);
            GroupCollection.Add(this);
            HoldFormation = hf;

        }
        public Group(Vector ul, Vector lr, VehicleType[] t, bool hf = true)
        {
            Id = IdCounter;
            IdCounter++;
            Controller.CreateGroup(ul, lr, t,Id);
            GroupCollection.Add(this);
            HoldFormation = hf;

        }


        public Group(Group[] gr,bool hf = true)
        {
            Id = IdCounter;
            IdCounter++;
            Controller.CreateGroup(gr,Id);
            GroupCollection.Add(this);
            HoldFormation = hf;
        }


        public void OnTick()
        {
            
            _units = Controller.GetVehiclesInGroup(Id);
            
            
            Vector c = new Vector(0,0);
            int count = 0;
            foreach (Vehicle v in Units)
            {
                c += new Vector(v.X, v.Y);
                count++;
            }


            _center = c / count;
           
            
            
            if (Motionless)
            {
                
                if (Orders.Count > 0)
                {

                    if (CurrentOrder == null)
                    {

                        TicksWithoutMotion = 0;
                        Motionless = false;
                        GroupOrder r = Orders.Dequeue();
                        Controller.GroupOrderExecution.Add(r);
                        CurrentOrder = r;
                        Console.WriteLine("Issued Order {0} for group {1} with id {2}", CurrentOrder.ToString(), Id,
                            CurrentOrder.Ordering);
                    }
                    else if(CurrentOrder.Executed)
                    {
                        TicksWithoutMotion = 0;
                        Motionless = false;
                        GroupOrder r = Orders.Dequeue();
                        Controller.GroupOrderExecution.Add(r);
                        CurrentOrder = r;
                        Console.WriteLine("Issued Order {0} for group {1} with id {2}", CurrentOrder.ToString(), Id,
                            CurrentOrder.Ordering);
                    }
                }
                else
                {

                   
                }
            }
            else
            {
                
                /*
                if (HoldFormation)
                {
                    try
                    {

                   /*
                    if (CurrentOrder is GroupMoveOrder)
                    {
                       
                   
                        foreach (Vehicle v in Units)
                        {
                            
                           
                            CellInfo cel = Controller.GetMapType(new Vector(v.X, v.Y));
                            if (!v.IsAerial)
                            {
                                if (cel.GroundSpeedModifier * v.MaxSpeed < (CurrentOrder as GroupMoveOrder).maxSpeed)
                                    (CurrentOrder as GroupMoveOrder).maxSpeed = cel.GroundSpeedModifier * v.MaxSpeed;
                            }
                            else
                            {
                                if (cel.AerialSpeedModifier * v.MaxSpeed < (CurrentOrder as GroupMoveOrder).maxSpeed)
                                    (CurrentOrder as GroupMoveOrder).maxSpeed = cel.AerialSpeedModifier * v.MaxSpeed;
                            }
                        }
                  
                        CurrentOrder.Execute();
                    }
                    
                    if (CurrentOrder is GroupMoveToOrder)
                    {
                        foreach (Vehicle v in Units)
                        {
                            
                            CellInfo cel = Controller.GetMapType(new Vector(v.X, v.Y));
                            if (!v.IsAerial)
                            {
                                if (cel.GroundSpeedModifier * v.MaxSpeed < (CurrentOrder as GroupMoveToOrder).maxSpeed)
                                    (CurrentOrder as GroupMoveToOrder).maxSpeed = cel.GroundSpeedModifier * v.MaxSpeed;
                            }
                            else
                            {
                                if (cel.AerialSpeedModifier * v.MaxSpeed < (CurrentOrder as GroupMoveToOrder).maxSpeed)
                                    (CurrentOrder as GroupMoveToOrder).maxSpeed = cel.AerialSpeedModifier * v.MaxSpeed;
                            }
                        }

                    }
                    }
                    catch (Exception e)
                    {
                       
                        Console.WriteLine(e);
                            
                    }
            
                }
        */

            }
    
            
        }
     

        public List<Vehicle> Units
        {
            get
            {
                return _units;
            }
        }

        public Vector Center
        {
            get{
             

                return _center;
            }
            
        }

        public Vector UpperLeft
        {
            get
            {
                List<Vehicle> u = Units;
                Vector temp = Controller.LowerRightCorner;
                foreach (Vehicle v in u)
                {
                    if (v.X < temp.x)
                    {
                        temp = new Vector(v.X,temp.y);
                    }
                    if (v.Y < temp.y)
                    {
                        temp = new Vector(temp.x,v.Y);
                    }
                }
                return temp;
            }
        }
        
        
        
       
        



   

        public double Health
        {
            get
            {
                List<Vehicle> u = Units;
                double temp = 0;

                foreach (Vehicle v in u)
                {
                    temp += v.Durability;
                }
                return temp;
            }
        }
        
        
        public Vector LowerRight
        {
            get
            {
                List<Vehicle> u = Units;
                Vector temp = Controller.UpperLeftCorner;
                
                foreach (Vehicle v in u)
                {
                    if (v.X > temp.x)
                    {
                        temp = new Vector(v.X,temp.y);
                    }
                    if (v.Y > temp.y)
                    {
                        temp = new Vector(temp.x,v.Y);
                    }
                }
                return temp;

            }  
        }
        
        


        public void Move(Vector point,double maxSpeed=0, bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                
                Controller.MoveGroup(Id, point, maxSpeed);
                Destination = point + Center;
                if (maxSpeed != 0)
                    Speed = maxSpeed;
                
            }
            else
            {
                Orders.Enqueue(new GroupMoveOrder(Id,point,maxSpeed));
            }
        }

        public void MoveTo(Vector point, double maxSpeed = 0, bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                Destination = point;
                if (maxSpeed != 0)
                    Speed = maxSpeed;
                Move(point - Center, maxSpeed);
                CurrentOrder = new GroupMoveToOrder(Id, point, maxSpeed);

            }
            else
            {
                Orders.Enqueue(new GroupMoveToOrder(Id,point,maxSpeed));
                
            }
        }
        


        public void Scale( double fac, Vector point,double maxspeed = 0,bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                Controller.ScaleGroup(Id, point, fac, maxspeed);
            }
            else
            {
                Orders.Enqueue(new GroupScaleOrder(Id,point,fac,maxspeed));
            }
        }
        
        public void Scale( double fac, double maxspeed = 0,bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                Controller.ScaleGroup(Id, Center, fac, maxspeed);
            }
            else
            {
                
                Orders.Enqueue(new GroupScaleOrder(Id,Center,fac,maxspeed));
            }
        }
        
        public void Rotate( double ang, Vector point,double maxspeed = 0,double maxangspeed=0, bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                Controller.RotateGroup(Id, point, ang, maxspeed, maxangspeed);
            }
            else
            {
                Orders.Enqueue(new GroupRotateOrder(Id,point,ang,maxspeed,maxangspeed));
            }
        }
        public void Rotate( double ang, double maxspeed = 0,double maxangspeed=0, bool urgent = false)
        {
            if (urgent)
            {
                Orders.Clear();
                Controller.RotateGroup(Id, Center, ang, maxspeed, maxangspeed);
                
            }
            else
            {
                
                Orders.Enqueue(new GroupRotateOrder(Id,Center,ang,maxspeed,maxangspeed));
            }
        }

   

    

       

    
    }
}