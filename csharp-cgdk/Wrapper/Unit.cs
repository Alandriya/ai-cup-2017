﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public class Unit
    {
        Vehicle Veh;
        public VehicleType Type;
        public long Id;

        public long PlayerId;
        public int[] GroupsId;
        public Vector Coords;

        public int RemainingCooldown;
        public int Cooldown;
        public int Durability;
        public bool Motionless;

        public bool IsAerial;
        public bool IsSelected;
        public double VisionRange;
        public double AerialAttackRange;
        public double AerialDefence;
        public double AerialAttack;
        public double GroundDefence;
        public double GroundAttack;
        public double MaxSpeed;
        public double Radius;
        

        public Unit(Vehicle v)
        {
            AerialAttackRange = v.AerialAttackRange;
            AerialDefence = v.AerialDefence;
            AerialAttack = v.AerialDamage;
            Cooldown = v.AttackCooldownTicks;
            Coords = new Vector(v.X, v.Y);
            Durability = v.Durability;
            GroundDefence = v.GroundDefence;
            GroundAttack = v.GroundDamage;
            GroupsId = v.Groups;
            Id = v.Id;
            IsAerial = v.IsAerial;
            IsSelected = v.IsSelected;
            MaxSpeed = v.MaxSpeed;
            PlayerId = v.PlayerId;
            Radius = v.Radius;
            RemainingCooldown = v.RemainingAttackCooldownTicks;
            Type = v.Type;
            Vehicle veh = v;
            VisionRange = v.VisionRange;
        }

        public double DistanceTo(Unit u)
        {
            return Veh.GetDistanceTo(u.Coords.x, u.Coords.y);
        }
        public double DistanceTo(double x, double y)
        {
            return Veh.GetDistanceTo(x, y);
        }


    }
}
