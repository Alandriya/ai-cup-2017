﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public class Squad //class for enemy's squads
    {
        private static int IdCounter = 0;
        public int Id;
        public double Health;
        //public double Speed;  //dynamic, not defined at first tick
        //public double Angle;  //dynamic, not defined at first tick
        public Vector UpperLeft;
        public Vector LowerRight;
        public Vector Center;
        public double Width;
        public double Height;

        public Squad(List <Coord> Component, double[,] HealthMap)
        {
            int size = 32;
            Id = IdCounter;
            IdCounter++;
            int num = 0;
            double sum = 0;
            Vector pos = new Vector(0, 0);
            foreach (var comp in Component)
            {
                num++;
                pos += new Vector(comp.x * (1024/size), comp.y * (1024 / size));
                sum += HealthMap[comp.x, comp.y];
            }
            Health = sum / num;
            Center = pos / num;

            Coord[] SortedWidth = (from c in Component orderby c.x select c).ToArray();
            Width = Math.Max(SortedWidth[SortedWidth.Length - 1].x - SortedWidth[0].x, 1) * (1024 / size);
            Coord[] SortedHeight = (from c in Component orderby c.y select c).ToArray();
            Height = Math.Max(SortedHeight[SortedHeight.Length - 1].y - SortedHeight[0].y, 1) * (1024 / size);

            UpperLeft = new Vector(SortedWidth[0].x * (1024 / size), SortedHeight[0].y * (1024 / size));
            LowerRight = new Vector(SortedWidth[SortedWidth.Length-1].x * (1024 / size), 
                SortedHeight[SortedHeight.Length-1].y * (1024 / size));
            //Speed = 0.0;
            //Angle = 0.0;
        }

    }
    public static partial class Controller
    {
        public static List<Squad> GetSquads()
        {
            List<Squad> Squads = new List<Squad>(); 
            List<Coord> Component;

            int size = 32;
            bool[,] Used = new bool[size, size];
            for (var i = 0; i < size; ++i)
                for (var j = 0; j < size; ++j)
                    Used[i, j] = false;

            for (var i = 0; i < size; ++i)
                for (var j = 0; j < size; ++j)
                    if (!Used[i, j])
                    {
                        Component = new List<Coord>();
                        DFS(ref Used, ref Component, i, j);
                        Console.WriteLine("OLOLO");
                        if (HealthMap[Component[0].x, Component[0].y] > 0.0)
                        {
                            Squads.Add(new Squad(Component, HealthMap));
                        }

                    }
            return Squads;
        }

        static void DFS(ref bool[,] Used, ref List<Coord> Component, int i, int j)
        {
            Used[i, j] = true;
            int size = 32;
            Component.Add(new Coord(i, j));
            if (i - 1 >= 0 && !Used[i - 1, j] && Math.Abs(HealthMap[i, j] - HealthMap[i-1, j]) < 0.1)
                DFS(ref Used, ref Component, i - 1, j);
            if (i + 1 < size && !Used[i + 1, j] && Math.Abs(HealthMap[i, j] - HealthMap[i + 1, j]) < 0.1)
                DFS(ref Used, ref Component, i + 1, j);
            if (j - 1 >= 0 && !Used[i, j - 1] && Math.Abs(HealthMap[i, j] - HealthMap[i, j - 1]) < 0.1)
                DFS(ref Used, ref Component, i, j - 1);
            if (j + 1 < size && !Used[i, j + 1] && Math.Abs(HealthMap[i, j] - HealthMap[i, j + 1]) < 0.1)
                DFS(ref Used, ref Component, i, j + 1);
        }

    }

}
