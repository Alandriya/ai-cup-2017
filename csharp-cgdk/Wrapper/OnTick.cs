﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public static partial class Controller
    {
        static Vector UpperLeft;
        static Vector LowerRight;
        public static void OnTick()
        {
            //update information about enemy
            Enemy = CurrentWorld.GetOpponentPlayer();
            UpdateHealthMap(ref HealthMap);


            if (AerialState == States.Formation)
            {
                //make nuclear fighters 
                Vector UpperLeft = Fighters.UpperLeft + new Vector(0, 52);
                Vector LowerRight = Fighters.LowerRight;
                NuclearFighters = new Group(UpperLeft, LowerRight, VehicleType.Fighter);


                //make common fighters
                UpperLeft = Fighters.UpperLeft;
                LowerRight = Fighters.LowerRight + new Vector(0, -2);
                CommonFighters = new Group(UpperLeft, LowerRight, VehicleType.Fighter);

                AerialState = States.Attack;
            }
            else if (AerialState == States.Attack)
            {
                //sending fighters and helicopters
                Vector position = EnemyFightersPos();
                Vector offset = new Vector(-30, -30); //TODO
                if (CurrentWorld.TickCount % 10 == 0)
                {
                    CommonFighters.MoveTo(position + offset);
                    Helicopters.MoveTo(position + offset);
                }


                //sending nuclear fighters
                //double mod = GetMapType(position).AerialRangeModifier;
                double mod = 0.6;

                offset = new Vector(115 * mod, 115 * mod); //TODO
                Vector aim = position + offset;
                NuclearFighters.MoveTo(position);


                //check if we can make a fucking NUCLEAR STRIKE
                if (CurrentPlayer.NextNuclearStrikeTickIndex == 0)
                {
                    Vehicle[] fighters = (from u in NuclearFighters.Units orderby u.GetDistanceTo(position.x, position.y) select u).ToArray();
                    NuclearStrike(aim, fighters[0].Id);
                }

            }

            if (GroundState == States.Formation)
            {
                Group tmp;

                //sorting ground formations by width
                Group[] GroundSortedWidth = (from g in Ground orderby g.Center.x select g).ToArray();
                Group[] GroundSortedHeight = (from g in Ground orderby g.Center.y select g).ToArray();

                if (Math.Abs(GroundSortedHeight[0].Center.y - GroundSortedHeight[1].Center.y) < 0.1
                    && GroundSortedHeight[0].Center.x > GroundSortedHeight[1].Center.x)
                {
                    tmp = GroundSortedHeight[0];
                    GroundSortedHeight[0] = GroundSortedHeight[1];
                    GroundSortedHeight[1] = tmp;
                }
                if (Math.Abs(GroundSortedHeight[0].Center.y - GroundSortedHeight[2].Center.y) < 0.1
                    && GroundSortedHeight[0].Center.x > GroundSortedHeight[2].Center.x)
                {
                    tmp = GroundSortedHeight[0];
                    GroundSortedHeight[0] = GroundSortedHeight[2];
                    GroundSortedHeight[2] = tmp;
                }
                if (Math.Abs(GroundSortedHeight[1].Center.y - GroundSortedHeight[2].Center.y) < 0.1
                    && GroundSortedHeight[1].Center.x > GroundSortedHeight[2].Center.x)
                {
                    tmp = GroundSortedHeight[1];
                    GroundSortedHeight[1] = GroundSortedHeight[2];
                    GroundSortedHeight[2] = tmp;
                }

                GroundSortedHeight[0].MoveTo(new Vector(50, 50));
                GroundSortedHeight[1].Move(new Vector(0, 50), maxSpeed: 0.3);
                GroundSortedHeight[1].MoveTo(new Vector(120, GroundSortedHeight[1].Center.y + 20), maxSpeed: 0.3);
                GroundSortedHeight[1].MoveTo(new Vector(120, 50), maxSpeed: 0.3);
                GroundSortedHeight[2].Move(new Vector(0, 50), maxSpeed: 0.3);
                GroundSortedHeight[2].MoveTo(new Vector(190, GroundSortedHeight[2].Center.y+20), maxSpeed: 0.3);
                GroundSortedHeight[2].MoveTo(new Vector(190, 50), maxSpeed: 0.3);

                GroundState = States.Rows;
            }
            else if (GroundState == States.Rows)
            {
                if(AllGround.Motionless)
                {
                    Group[] Rows = new Group[10];

                    for (int i = 1; i < 10; i++)
                    {
                        UpperLeft = new Vector(19, 19) + new Vector(0, 8 * i);
                        LowerRight = new Vector(19, 19) + new Vector(AllGround.LowerRight.x, 8 * i + 8);
                        Rows[i] = new Group(UpperLeft, LowerRight);
                    }
                    
                    for(int i = 9; i > 0; i--)
                    {
                        Rows[i].Move(new Vector(0, 8*i));
                    }
                    GroundFormation[0] = AllGround;
                    GroundState = States.Attack;
                }
            }
            else if (GroundState == States.Attack)
            {
                Vector position;

                ////sending ground formations
                //position = new Vector(250, 250); //TODO
                //GroundFormation[0].MoveTo(position);

                ////check if enemy is going to make nuclear strike
                //if (Enemy.NextNuclearStrikeTickIndex != -1)
                //{
                //    position = new Vector(Enemy.NextNuclearStrikeX, Enemy.NextNuclearStrikeY);
                //    List<Group> GroupsInDanger = FindNearestGroups(position, radius: 100.0);
                //    foreach (var g in GroupsInDanger)
                //    {
                //        g.Scale(5, position, urgent: true);
                //    }
                //}
            }
        }
    }
}
