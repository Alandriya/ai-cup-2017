﻿using System.Threading;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders
{
    public class GroupMoveToOrder:GroupOrder
    {

        public double maxSpeed;
        private Vector point;
    

        public GroupMoveToOrder(int d,Vector pt, double ms = 0):base(true)
        {
            maxSpeed = ms;
            point = pt;
            id = d;
        }
        public override void Execute()
        {
            base.Execute();
            Controller.MoveGroup(id,point - Group.GroupCollection[id-1].Center,maxSpeed);
            Group.GroupCollection[id - 1].Destination = point;
            Group.GroupCollection[id - 1].Speed = maxSpeed;


        }
        
        public override string ToString()
        {
            return ("MoveToOrder");
        }
    }
}