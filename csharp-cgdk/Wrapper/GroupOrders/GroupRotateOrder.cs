﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders
{
    public class GroupRotateOrder:GroupOrder
    {

        private double maxSpeed;
        private Vector point;
        private double Angle;
        private double maxAngSpeed;
   

        public GroupRotateOrder(int d,Vector pt,double ang, double ms = 0, double maxang=0):base(true)
        {
            maxSpeed = ms;
            point = pt;
            id = d;
            Angle = ang;
            maxAngSpeed = maxang;
        }
        public override void Execute()
        {
            base.Execute();
            Group.GroupCollection[id - 1].TicksWithoutMotion = 0;
            Group.GroupCollection[id - 1].Motionless = false;
            Controller.RotateGroup(id,point,Angle,maxSpeed,maxAngSpeed);
        }
        
        
        public override string ToString()
        {
            return ("RotateOrder");
        }
    }
}