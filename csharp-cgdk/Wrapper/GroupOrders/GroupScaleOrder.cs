﻿using System.Security.Cryptography.X509Certificates;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders
{
    public class GroupScaleOrder:GroupOrder
    {

        private double maxSpeed;
        private Vector point;
        private double factor;
        
        private bool centered;

        public GroupScaleOrder(int d,Vector pt, double fac, double ms=0,bool cntr = true):base(true)
        {
            maxSpeed = ms;
            factor = fac;
            point = pt;
            centered = cntr;
            id = d;
        }
        public override void Execute()
        {
            base.Execute();
            Group.GroupCollection[id - 1].TicksWithoutMotion = 0;
            Group.GroupCollection[id - 1].Motionless = false;
            if(!centered)
                Controller.ScaleGroup(id,point,factor,maxSpeed);
            else
                Controller.ScaleGroup(id,Group.GroupCollection[id-1].Center,factor,maxSpeed);
        }
        
        public override string ToString()
        {
            return ("ScaleOrder");
        }
    }
}