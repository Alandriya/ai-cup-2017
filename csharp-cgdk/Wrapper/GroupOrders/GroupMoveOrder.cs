﻿using System;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders
{
    public class GroupMoveOrder:GroupOrder
    {

       public double maxSpeed;
        private Vector point;
        

        public GroupMoveOrder(int d,Vector pt, double ms = 0):base(true)
        {
            maxSpeed = ms;
            point = pt;
            id = d;
        }
        public override void Execute()
        {
            base.Execute();
            Controller.MoveGroup(id,point,maxSpeed);
            Group.GroupCollection[id - 1].Destination = point+Group.GroupCollection[id - 1].Center;
            Group.GroupCollection[id - 1].Speed = maxSpeed;
        }

        public override String ToString()
        {
            return ("MoveOrder");
        }
    }
}