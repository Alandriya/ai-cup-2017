﻿using System;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;


namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.GroupOrders
{
    public class GroupOrder
    {


        protected int id = 0;
        protected static  int OrderingCount = 0;
        public bool Executed = false;
    
        public GroupOrder(bool obeyorder = true)
        {
            Ordering = OrderingCount;
            OrderingCount++;
        }
        
        public int Ordering = 0;
            
        public virtual void Execute()
        {
            Group.GroupCollection[id - 1].TicksWithoutMotion = 0;
            Group.GroupCollection[id - 1].Motionless = false;
            Console.WriteLine("Executing order from group {0} with id {1}",id,Ordering);
            Executed = true;

        }
        
        
        
    }
}