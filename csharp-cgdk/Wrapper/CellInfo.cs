﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public struct CellInfo
    {
        public double AerialSpeedModifier;
        public double GroundSpeedModifier;
        public double AerialStealthModifier;
        public double GroundStealthModifier;
        public double AerialRangeModifier;
        public double GroundRangeModifier;
        public CellInfo(TerrainType Terrain, WeatherType Weather)
        {
            switch (Terrain)
            {
                case TerrainType.Forest:
                    GroundRangeModifier = 0.8;
                    GroundSpeedModifier = 0.8;
                    GroundStealthModifier = 0.6;
                    break;
                case TerrainType.Swamp:
                    GroundRangeModifier = 1.0;
                    GroundSpeedModifier = 0.6;
                    GroundStealthModifier = 1.0;
                    break;
                default:
                    GroundRangeModifier = 1.0;
                    GroundSpeedModifier = 1.0;
                    GroundStealthModifier = 1.0;
                    break;
            }

            switch (Weather)
            {
                case WeatherType.Cloud:
                    AerialRangeModifier = 0.8;
                    AerialSpeedModifier = 0.8;
                    AerialStealthModifier = 0.8;
                    break;
                case WeatherType.Rain:
                    AerialRangeModifier = 0.6;
                    AerialSpeedModifier = 0.6;
                    AerialStealthModifier = 0.6;
                    break;
                default:
                    AerialRangeModifier = 1.0;
                    AerialSpeedModifier = 1.0;
                    AerialStealthModifier = 1.0;
                    break;
            }

        }
    }
}
