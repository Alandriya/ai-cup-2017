﻿
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class NuclearStrikeOrder:Order
    {

        private long unitId;
        private Vector point;

        public NuclearStrikeOrder(long num, Vector pt)
        {
            unitId = num;
            point = pt;
        }

        public override void SetMove(ref Move target)
        {
            target.Action = ActionType.TacticalNuclearStrike;
            target.X = point.x;
            target.Y = point.y;
            target.VehicleId = unitId;
            base.SetMove(ref target);
        }
    }
}