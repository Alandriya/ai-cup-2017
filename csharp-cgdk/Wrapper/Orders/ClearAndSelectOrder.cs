﻿using System.Diagnostics;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class ClearAndSelectOrder:Order
    {

        private Vector upperLeft;
        private Vector lowerRight;
       public  int groupNumber = -1;
        private bool gotVehicle = false;
        private VehicleType vehType;

        public ClearAndSelectOrder(Vector ul, Vector lr,int gr = -1)
        {
            upperLeft = ul;
            lowerRight = lr;
            groupNumber = gr;
        }
        
        
        public ClearAndSelectOrder(Vector ul, Vector lr,VehicleType tp, int gr = -1)
        {
            upperLeft = ul;
            lowerRight = lr;
            groupNumber = gr;
            vehType = tp;
            gotVehicle = true;
        }

        public override void SetMove(ref Move target)
        {
            
            target.Action = ActionType.ClearAndSelect;
            if (groupNumber != -1)
                target.Group = groupNumber;
            if (gotVehicle)
                target.VehicleType = vehType;
            target.Top = upperLeft.y;
            target.Left = upperLeft.x;
            target.Bottom = lowerRight.y;
            target.Right = lowerRight.x;
            
            

        }


     
    }
}