﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class DismissOrder:Order
    {
        private int groupNumber = 0;
        public DismissOrder(int gr)
        {
            groupNumber = gr;
        }

        public override void SetMove(ref Move target)
        {
            base.SetMove(ref target);
            target.Action = ActionType.Dismiss;
            target.Group = groupNumber;
        }
    }
}