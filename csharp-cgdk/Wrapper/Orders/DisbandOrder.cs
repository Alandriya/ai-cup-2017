﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class DisbandOrder:Order
    {
        private int groupNumber = 0;

        public DisbandOrder(int k)
        {
            groupNumber = k;
        }
        
        public override void SetMove(ref Move target)
        {
            base.SetMove(ref target);
            target.Action = ActionType.Disband;
            target.Group = groupNumber;
        }
    }
}