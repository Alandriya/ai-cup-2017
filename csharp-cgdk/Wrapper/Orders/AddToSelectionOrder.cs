﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;


namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class AddToSelectionOrder:Order
    {
        private Vector upperLeft;
        private Vector lowerRight;
        private int groupNumber = -1;
        private bool gotVehicle = false;
        private VehicleType vehType;

        public AddToSelectionOrder(Vector ul, Vector lr,int gr = -1)
        {
            upperLeft = ul;
            lowerRight = lr;
            groupNumber = gr;
            
        }
        
        
        public AddToSelectionOrder(Vector ul, Vector lr,VehicleType tp, int gr = -1)
        {
            upperLeft = ul;
            lowerRight = lr;
            groupNumber = gr;
            vehType = tp;
        }

        public override void SetMove(ref Move target)
        {
            base.SetMove(ref target);
            target.Action = ActionType.AddToSelection;
            if (groupNumber != -1)
                target.Group = groupNumber;
            if (gotVehicle)
                target.VehicleType = vehType;
            target.Top = upperLeft.y;
            target.Left = upperLeft.x;
            target.Bottom = lowerRight.y;
            target.Right = lowerRight.x;

        }
    }
}