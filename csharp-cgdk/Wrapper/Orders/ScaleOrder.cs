﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class ScaleOrder:Order
    {
        private double factor;
        private Vector point;
        private double maxSpeed;
      

        public ScaleOrder(double fac, Vector pt, double max = 0)
        {
            factor = fac;
            point = pt;
            maxSpeed = max;
           
        }

        public override void SetMove(ref Move target)
        {
            target.Action = ActionType.Scale;
            target.Factor = factor;
            target.X = point.x;
            target.Y = point.y;
            if (maxSpeed > 0)
                target.MaxSpeed = maxSpeed;
   

        }
    }
}