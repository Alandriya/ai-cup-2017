﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;


namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class MoveOrder:Order
    {
        private double x;
        private double y;
        private double maxSpeed = -1;

        public MoveOrder(double x0, double y0, double ms = -1)
        {
            maxSpeed = ms;
            x = x0;
            y = y0;
        }
        public MoveOrder(Vector m, double ms = -1)
        {
            maxSpeed = ms;
            x = m.x;
            y = m.y;
        }

        public override void SetMove(ref Move target)
        {
           
            target.Action = ActionType.Move;
            target.X = x;
            target.Y = y;
            if(maxSpeed>0)
            target.MaxSpeed = maxSpeed;
        }
    }
}