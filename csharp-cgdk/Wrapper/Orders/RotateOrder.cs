﻿using System.Reflection;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders
{
    public class RotateOrder:Order
    {
        private double angle;
        private Vector point;
        private double maxSpeed;
        private double maxAngSpeed;

        public RotateOrder(double ang, Vector pt, double max = 0, double maxas = 0)
        {
            angle = ang;
            point = pt;
            
            maxSpeed = max;
            maxAngSpeed = maxas;
        }

        public override void SetMove(ref Move target)
        {
            target.Action = ActionType.Rotate;
            target.Angle = angle;
            target.X = point.x;
            target.Y = point.y;
            if (maxSpeed > 0)
                target.MaxSpeed = maxSpeed;
            if (maxAngSpeed > 0)
                target.MaxAngularSpeed = maxAngSpeed;

        }
    }
}