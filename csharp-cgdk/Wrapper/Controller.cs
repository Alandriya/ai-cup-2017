using System;
using System.Linq;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Collections.Generic;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper
{
    public static partial class Controller
    {
        enum States { MoveAll, Formation, Attack, Rows};
        static Group Fighters;
        static Group Helicopters;
        static Group Tanks;
        static Group IFVs;
        static Group ARRVs;

        static Group AllGround;
        static Group All;
        static Group NuclearFighters;
        static Group CommonFighters;

        static Group[] Ground;
        static Group[] GroundFormation;
        static Group[] Aerial;

        static States GroundState;
        static States AerialState;
        static double[,] HealthMap;
        static private Player Enemy;

        public static void OnStart()
        {
            Console.WriteLine("Initilization complete");
            Console.WriteLine("Game field is {0} height and {1} width", LowerRightCorner.y, LowerRightCorner.x);

            //getting information about enemy
            HealthMap = MakeHealthMap();

            //making groups by type and setting state to formation
            GroundState = States.Formation;
            AerialState = States.Formation;
            Fighters = new Group(UpperLeftCorner, LowerRightCorner, VehicleType.Fighter);
            Helicopters = new Group(UpperLeftCorner, LowerRightCorner, VehicleType.Helicopter);

            Vector position = EnemyFightersPos();
            Vector offset = new Vector(-30, -30);
            Helicopters.MoveTo(position + offset);

            Tanks = new Group(UpperLeftCorner, LowerRightCorner, VehicleType.Tank);
            IFVs = new Group(UpperLeftCorner, LowerRightCorner, VehicleType.Ifv);
            ARRVs = new Group(UpperLeftCorner, LowerRightCorner, VehicleType.Arrv);

            Ground = new Group[]{ Tanks, IFVs, ARRVs };
            AllGround = new Group(Ground);

            All = new Group(UpperLeftCorner, LowerRightCorner);
            GroundFormation = new Group[1];

        }
        private static Vector EnemyFightersPos()
        {
            int count = 0;
            Vector position = new Vector(0, 0);
            List<Vehicle> EnemyFigh = new List<Vehicle>();
            foreach (Vehicle v in Vehicles.Values)
            {
                if (v.PlayerId != 1 && v.IsAerial && v.GroundDamage < 0.1)
                {
                    EnemyFigh.Add(v);
                    position += new Vector(v.X, v.Y);
                    count++;
                }
            }
            position /= count;
            return position;
        }
        private static List<Group> FindNearestGroups(Vector position, double radius)
        {
            List<Group> Nearest = new List<Group>();
            foreach (var g in Ground)
            {
                if ((g.Center - position).magnitude < radius)
                    Nearest.Add(g);
            }
            foreach (var g in Aerial)
            {
                if ((g.Center - position).magnitude < radius)
                    Nearest.Add(g);
            }
            return Nearest;
        }

    }
}
