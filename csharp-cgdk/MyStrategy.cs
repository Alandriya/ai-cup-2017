using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Wrapper.Orders;
using System.Collections.Generic;
using System.IO.Ports;
using System;
using System.Runtime.Serialization.Formatters;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk {
    public sealed class MyStrategy : IStrategy
    {
        
        

        private static  Queue<Order> Orders = new Queue<Order>();

        private static List<int> CoolDowns = new List<int>();
        public static bool TurnAvailable = false;

        
        
        public static void AddOrder(Order set)
        {
            Orders.Enqueue(set);
           
        }
        
        //
        public void Move(Player me, World world, Game game, Move move)
        {
            Controller.Sync(ref world, ref game, ref me);
            //Если можно запустить
            if (world.TickIndex==0)
            {
                Console.WriteLine("{0},{1}",world.Width,world.Height);
          
                Controller.OnStart();
                
            }
            if (CoolDowns.Count < 12)
            {

                TurnAvailable = true;
                //Если очередь не пуста возьмем приказ оттуда
                if (Orders.Count > 0)
                {
                  
                    Orders.Dequeue().SetMove(ref move);
                    
                    CoolDowns.Add(60);
                }
                else
                {
                    //Иначе проделаем свой тик
                   
                 
                    Controller.OnTick();
                    
                    
                    //Если тик что-то дал
                    if (Orders.Count > 0)
                    {
                        
                        
                        Orders.Dequeue().SetMove(ref move);
                        CoolDowns.Add(60);
                    }
                    else
                    {
                        //Ничего не делаем если тик не дал ничего
                        
                       
                        
                    }
                }
            }
            else
            {

                TurnAvailable = false;
            }
            
            
            
            //Чистим кулдауны
            List<int> forDelete = new List<int>();

            for (int i = 0; i < CoolDowns.Count; i++)
            {
                CoolDowns[i]--;
                if(CoolDowns[i]<0)
                forDelete.Add(i);
            }
            foreach (var i in forDelete)
            {
                CoolDowns.RemoveAt(i);
            }

            
          
            


        }
        
        
        
        
    }
}